import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { LOAD_BOOKS } from "../GraphQL/Queries";
import {
    Card,
    CardContent,
    CardMedia,
    Typography,
    Grid,
    CircularProgress,
    Container,
    TextField,
    List,
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
    IconButton,
} from "@mui/material";
import { makeStyles } from "@mui/styles"
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

// Custom styles using muulish fonts
const useStyles = makeStyles(() => ({
    container: {
        paddingTop: "5%",
        fontFamily: "'Mulish', sans-serif",
    },
    searchInput: {
        marginBottom: "20px",
    },
    listItem: {
        backgroundColor: "#CFFAFA",
        marginBottom: "16px",
    },
    card: {
        backgroundColor: "#FFE6DC",
    },
    cardMedia: {
        height: 140,
    },
    cardContent: {
        backgroundColor: "#5ACCCC",
        color: "#335C6E",
    },
    title: {
        fontFamily: "'Mulish', sans-serif",
        fontWeight: 700,
    },
    subTitle: {
        fontFamily: "'Mulish', sans-serif",
        fontWeight: 400,
    },
    readingListTitle: {
        marginTop: "32px",
        fontFamily: "'Mulish', sans-serif",
        color: "#FABD33",
    },
    button: {
        color: "#F76434",
    },
    icon: {
        color: "#FAAD00",
    },
}));

function GetBooks() {
    //define states 
    const classes = useStyles();
    const { error, loading, data } = useQuery(LOAD_BOOKS);
    const [books, setBooks] = useState([]);
    const [searchQuery, setSearchQuery] = useState("");
    const [searchResults, setSearchResults] = useState([]);
    const [readingList, setReadingList] = useState([]);



    useEffect(() => {
        if (data) {
            setBooks(
                data.books.map((book) => ({
                    ...book,
                    coverPhotoURL: require(`../components/${book.coverPhotoURL}`),//reroute the image url to the required directory
                }))
            );
        }
    }, [data]);

    if (loading) return <CircularProgress />;
    if (error) return <Typography>Error: {error.message}</Typography>;

    const handleSearchChange = (event) => {
        const query = event.target.value.toLowerCase();
        setSearchQuery(query);
        if (query === "") {
            setSearchResults([]);
        } else {
            const filtered = books.filter(
                (book) =>
                    book.title.toLowerCase().includes(query) ||
                    book.author.toLowerCase().includes(query)
            );
            setSearchResults(filtered.slice(0, 5));// display top 5 of the search results 
        }
    };
     //function to store selected reading options
    const handleAddToReadingList = (book) => {
        if (!readingList.includes(book)) {
            setReadingList((prevList) => [...prevList, book]);
        }
    };
    //removing books in the reading list
    const handleRemoveFromReadingList = (book) => {
        setReadingList((prevList) => prevList.filter((item) => item !== book));
    };

    return (
        <Container className={classes.container}>
            <TextField
                label="Search Books"
                variant="outlined"
                fullWidth
                value={searchQuery}
                onChange={handleSearchChange}
                className={classes.searchInput}
            />
            <List>
                {searchResults.map((book, index) => ( //get search results
                    <ListItem key={index} className={classes.listItem}>
                        <Card className={classes.card}>
                            <Grid container spacing={2}>
                                <Grid item xs={3}>
                                    <CardMedia
                                        component="img"
                                        height="100"
                                        image={book.coverPhotoURL}
                                        alt={book.title}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <CardContent>
                                        <Typography variant="h6" className={classes.title}>
                                            {book.title}
                                        </Typography>
                                        <Typography variant="body1" className={classes.subTitle}>
                                            Author: {book.author}
                                        </Typography>
                                    </CardContent>
                                </Grid>
                                <Grid item xs={3}>
                                    <ListItemSecondaryAction>
                                        <IconButton
                                            edge="end"
                                            aria-label="add"
                                            onClick={() => handleAddToReadingList(book)}
                                            className={classes.button}
                                        >
                                            <AddIcon className={classes.icon} />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </Grid>
                            </Grid>
                        </Card>
                    </ListItem>
                ))}
            </List>
            <Typography variant="h5" gutterBottom className={classes.readingListTitle}>
                Books on Reading List
            </Typography>
            <List>
                {readingList.map((book, index) => (
                    <ListItem key={index}>
                        <ListItemText
                            primary={book.title}
                            secondary={`Author: ${book.author}`}
                        />
                        <ListItemSecondaryAction>
                            <IconButton
                                edge="end"
                                aria-label="remove"
                                onClick={() => handleRemoveFromReadingList(book)}
                                className={classes.button}
                            >
                                <RemoveIcon className={classes.icon} />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                ))}
            </List>

            <Typography variant="h4" className={classes.readingListTitle}>
                Books For You!
            </Typography>
            <Grid container spacing={3}>
                {books.map((book, index) => (
                    <Grid item key={index} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                            <CardMedia
                                component="img"
                                height="140"
                                image={book.coverPhotoURL}
                                alt={book.title}
                                className={classes.cardMedia}
                            />
                            <CardContent className={classes.cardContent}>
                                <Typography variant="h5" className={classes.title}>
                                    {book.title}
                                </Typography>
                                <Typography variant="body2" className={classes.subTitle}>
                                    Author: {book.author}
                                </Typography>
                                <Typography variant="body2" className={classes.subTitle}>
                                    Reading Level: {book.readingLevel}
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
}

export default GetBooks;
