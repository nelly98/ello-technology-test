
import './App.css';
import {ApolloClient,InMemoryCache,ApolloProvider,HttpLink,from } from '@apollo/client';
import {onError} from '@apollo/client/link/error'
import { GraphQLErrors } from 'graphql';
import GetBooks from './components/GetBooks';



const errorLink =onError(({GraphQLErrors, networkError})=>{
  if (GraphQLErrors){
    GraphQLErrors.map(({message,location,path})=>{
      alert(`graphql error ${message}`);
    });
  }
})
const link =from([
  errorLink,
  new HttpLink({uri:'http://localhost:4000/'})
])




const client = new ApolloClient ({
  cache:new InMemoryCache(),
  link:link
})


function App() {
  return <ApolloProvider client={client}>
    {" "}
    <GetBooks/>

  </ApolloProvider>;
}
export default App;
